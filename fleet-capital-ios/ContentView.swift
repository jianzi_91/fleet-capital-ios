//
//  ContentView.swift
//  fleet-capital-ios
//
//  Created by Jianzi Lu on 7/9/20.
//  Copyright © 2020 Plus65. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    @State var current: Double = 0
    @State var point: Double = 0
    @State var alert: Bool = false
    @State var target = Double.random(in:1...99)
    
    var body: some View {
        VStack {
            Text("Try to get \(Int(target)) \n")
                .multilineTextAlignment(.center)
                .padding(.top)
            HStack {
                Spacer()
                Text("1")
                Slider(value: $current, in: 0...100, step: 1)
                Text("100")
                Spacer()
            }
            
            Text("Slide the slider \n")
//            Button("Reset", action: {self.celcius = 0})
            HStack {
                Button("Hit Me!", action: {
                    let initialPoints  = 100.00
                    let penalty = self.target >= self.current ? self.target - self.current : self.current - self.target
                    let pointsObtained = initialPoints - penalty
                    
                    self.point += pointsObtained
                    self.alert = true
                    self.target = Double.random(in: 1...99)
                }).padding(.trailing).alert(isPresented: $alert) { () -> Alert in
                    
                    
                    
                    return Alert(title: Text("Hooray"), message: Text("You Hit \(Int(current)) Your current marks is \(Int(point.rounded()))"), dismissButton: .default(Text("Dismiss")))
                }
                Button("Reset!", action: {
                    self.current = 0
                    self.point = 0
                    self.target = Double.random(in: 1...99)
                }).padding(.leading).foregroundColor(Color.red)
            }
            Text("\nCurrent points: \(Int(point.rounded()))")
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView().previewLayout(.fixed(width: 568, height: 320))
    }
}
